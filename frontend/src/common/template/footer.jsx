import React from 'react'

export default props => (
    <footer className = 'main-footer'>
    <strong>
        Criador por Ramon Mercês 
        <a href='www.ramon_merces.com' target='_blank'> RCM</a>.
    </strong>
    
    </footer>
)