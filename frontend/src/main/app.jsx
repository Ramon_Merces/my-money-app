import '../common/template/dependencies'
import React from 'react'
import { Route, Redirect, HashRouter as Router } from 'react-router';



import Header from '../common/template/header'
import SideBar from '../common/template/sideBar'
import Footer from '../common/template/footer'
import Routes from './routes'
import BillingCycle from '../billingCycle/billingCycle'
import Dashboard from '../dashboard/dashboard'
import Messages from '../common/msg/msgs'

export default props => (
    <div className='wrapper'>
        <Header />
        <SideBar />
        <div className='content-wrapper'>
        <Routes />
        </div>
        <Footer />
        <Messages />
    </div>
)